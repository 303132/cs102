package cs102.week02;

public class Car {
    public double odometer;
    public String brand;
    public String color;
    public int gear;
    private double totalHoursTraveled;

    public Car(String ibrand, String icolor) {
        odometer = 0;
        brand = ibrand;
        color = icolor;
        gear = 0;
        totalHoursTraveled = 0;
    }

    public Car(String ibrand) {
        odometer = 0;
        brand = ibrand;
        color = "White";
        gear = 0;
        totalHoursTraveled = 0;
    }

    public void incrementGear() {
        if (gear < 5) {
            gear += 1;
        } else {
            System.out.println("Cannot increment gear any more. It is already at " + gear +".");
        }
    }

    public void decrementGear() {
        if (gear > 0) {
            gear -= 1;
        } else {
            System.out.println("Cannot decrement gear any more. It is already at " + gear +".");
        }
    }

    public void drive(double numberOfHoursTraveled, double kmTraveledPerHour) {
        odometer += numberOfHoursTraveled * kmTraveledPerHour;
        totalHoursTraveled += numberOfHoursTraveled;
    }

    public double getOdometer() {
        return odometer;
    }

    public void setOdometer(double newOdometer) {
        odometer = newOdometer;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String newBrand) {
        brand = newBrand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String newColor) {
        color = newColor;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int newGear) {
        if ((newGear >= 0) && (newGear <= 5 )) {
            gear = newGear;
        } else {
            System.out.println("Cannot set gear to " + newGear + ". Its value must be between 0 and 5.");
        }
    }

    public void display() {
        System.out.println("The car has the brand "  + brand + " and its color is " + color +
                ". It has traveled " + odometer + " kms so far. It's at gear " + gear +
                ". It has traveled for " + totalHoursTraveled + " hours with and average speed of " + getAverageSpeed() + ".");
    }

    public double getTotalHoursTraveled() {
        return totalHoursTraveled;
    }

    private double getAverageSpeed() {
        if (totalHoursTraveled > 0) {
            return odometer / totalHoursTraveled;
        }
        else {
            return 0;}
        }
    }

